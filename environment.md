# Setting up your environment

## Docker

You can easily set up a working Framadate with the following instructions:
* Make sure no web server (Apache, Nginx, ...) is already running on your system on port 80
* Install the `docker-ce` and `docker-compose` tools
* `git clone git@framagit.org:framasoft/framadate.git` to your work space
* checkout the `develop` branch
* docker-compose. According to your version (launch `docker-compose -v`)
  * version 1.24.0 : `docker-compose -d up` (prefix by `sudo` if your user is not inside the `docker` group).
  * version 1.17.1 : `docker-compose up -d` (prefix by `sudo` if your user is not inside the `docker` group).
* Access [`http://localhost`](http://localhost) into your browser