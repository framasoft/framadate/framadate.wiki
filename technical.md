# Main language
The main language of framadate is PHP.

# Treeview of the project
To understand what is the purpose of each page, may be you can look in the [Tree view](Treeview) documentation.

# Smarty - template engine
The template engine [Smarty](https://www.smarty.net) is used for the dynamique pages.

# Database
The database is available through Doctrine/DBAL which is configured inside `app/inc/init.php`. This abstraction layer allows us to use the [Schema representation](https://www.doctrine-project.org/projects/doctrine-dbal/en/2.7/reference/schema-representation.html#schema-representation). We also use Doctrine Migrations for migration.

This file `init.php` is included in each page.

-------------
[Back to homepage](home)