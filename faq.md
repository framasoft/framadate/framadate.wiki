# Frequently Asked Questions

### I have a blank page after installing Framadate!
* Did you check that `php-intl` is installed or activated ?
* Is `htaccess.txt` renamed to `.htaccess` and if you have access to Apache, do you have `AllowOverride All` inside your vhost file ?
* Do the access rights for the Framadate seem right ?
* Did you double-check credentials for database ?
* Does everything on `admin/check.php` seems all right ?

### I have a display issue with Internet Explorer 11

See https://framagit.org/framasoft/framadate/framadate/issues/313 and **please** upgrade to a proper browser.

### How to disable sending emails for polls

Set `use_smtp` to `false` inside `app/inc/config.php`.

### Some date strings are not translated

Please make sure your server has the corresponding locale configured and installed. On Debian-like servers, it's `dpkg reconfigure locales`. You need `%locale% UTF-8`.