The install process consists of three phases:
- [Installing requirements](install/Requirements)
- [Configuring](install/Configure)
- [Installing](install/Install)
- [Tweak the configuration](install/Tweak)