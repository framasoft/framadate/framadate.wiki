Updating Framadate consists of four things:
* Getting the latest version zip and unzip it
* Replacing all previous files with the new files now extracted
* Checking `tpl/admin/config.tpl` for any new config options in your config file at `app/inc/config.php`
* Removing all template cached files inside `tpl_c/`
* Browsing to `admin/migration.php` to make the new eventual migration execute