You should backups three things:
* Most importantly, the framadate database
  * If you're using MySQL, you can dump it and save it on regular basis - with a cron system
  * If you're using PostgreSQL, you can also dump it and save it regularly or you can use [Barman](https://www.pgbarman.org) for easy backups. 
* The `app/inc/config.php` file (holds your database connection credentials)
* And eventually the `admin/.htpasswd` file protecting your admin section