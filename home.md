> If you want to edit the wiki, please click on request access [on the project's page](https://framagit.org/framasoft/framadate/framadate)

# [FAQ](FAQ)

# Installation

The install process consists of four phases:
- [Installing requirements](Install/Requirements)
- [Configuring the database](Install/Database)
- [Installing Framadate](Install/Install)
- [Configuring Framadate](Install/Configure)

You can also easily install Framadate through:

[![YunoHost](https://camo.githubusercontent.com/a657193184c779d657ef3f1e2834590d6615a950/68747470733a2f2f696e7374616c6c2d6170702e79756e6f686f73742e6f72672f696e7374616c6c2d776974682d79756e6f686f73742e706e67)](https://install-app.yunohost.org/?app=opensondage)
[![Softaculous](/uploads/3df45a0b89fe9c44c7dd135b67b8f1ff/softaculous_framadate.png)](https://www.softaculous.com/softaculous/apps/polls/Framadate)

# Maintenance
- [Maintenance mode](Maintenance/Mode)
- [Updating](Maintenance/Updating)
- [Backups](Maintenance/Backups)

# Development
* [Setting up your environment](environment)
* [Technical](technical)
* [Treeview](treeview)
* [Dealing with locales](locales)

# Contributing
* [Coding](coding)
* [Translating](translating)
