# Translations

Translations are located inside the `locale` folder. Each language is located inside a different JSON file organized by section.

We recommend to not change files directly !
If you have changes to make, or adding a new language, head over to https://weblate.framasoft.org/projects/framadate/

However, if you have changes to apply to the English translation, you **have to** edit the strings directly into the code and then edit the english JSON file to match your key/value changes.

If you add new sentences to translate, add them to the english JSON file (You can add new strings by doing `make add-key-locales "key:string"` (Example: `make add-key-locales "3rd section:This is a new string."`)) and include it in your merge request, they will be pushed to Zanata (our translation platform). Then, a manual action from our side (ie the repository owners) is needed to pull back the translations in the repository

-------------
[Back to homepage](home)