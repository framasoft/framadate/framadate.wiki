# How to contribute with code

## On your side

1. Create an account on [https://framagit.org](https://framagit.org)
1. Fork the main project: Go to the Framadate Main Project Site and press fork.
1. Create a branch named `feature/[Description]` or `bug/[description]` from branch **develop** where [Description] is an english short description of what your patch does.
1. Make commits inside your branch
1. Push your branch inside your fork
1. Create a merge request towards **develop**
1. Check the « Accept edits from maintainers » checkbox

Note : If a while has passed since you forked the develop branch, there may be conflicts. In that case, don't forget to rebase your forked branch before pushing it for the Merge Request.

## We handle the rest

1. Someone will review your work: try to make it easier by organizing your commits
1. If there's remarks on your work, the reviewer will comment on the merge request
1. If the merge request looks good someone will merge your work inside the **develop** branch.

## Corrections following a review

The merge request review might indicate you to make some change for it to be acceptable.
You can do these corrections inside your own branch, that will add it to the merge request.

## Understand the code

A wiki page is dedicated to discover Framadate's code structure : [Technical document](technical).

-------------
[Back to homepage](home)