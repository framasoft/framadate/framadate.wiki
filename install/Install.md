# Install Framadate

You need to get inside your server's web directory, such as `/var/www/html` on Debian (or Ubuntu).

```bash
cd /var/www/html
```

You can install the stable release or the development version. If you don't know what to choose, choose the stable release.

## Stable release

Download and unzip the latest stable release of Framadate. Head over to https://framagit.org/framasoft/framadate/framadate/tags and click « Download release » on the latest release. You can also copy the address of the file and download it with wget directly on your server.

> This part is no longer possible because we moved the Framadate repository and it broke CI release deployment. It [should be back](https://gitlab.com/gitlab-org/gitlab-ce/issues/30548) at the end of the year with Gitlab 11.6
> ```bash
> wget https://framasoft.frama.io/framadate/latest.zip -P /tmp
> sudo -u www-data unzip /tmp/latest.zip
> ```

You can delete the archive:
```bash
rm /tmp/latest.zip
```

Set the proper permissions on the newly extracted folder
```bash
chown www-data: framadate
cd framadate
```

## Development version

You will need Composer, php7.0-mbstring and php7.0-xml (On Debian 9: `apt install php7.0-mbstring php7.0-xml`, then [install Composer](https://getcomposer.org/doc/00-intro.md))

Clone the repository:
```bash
sudo -u www-data git clone https://framagit.org/framasoft/framadate/framadate.git/
```

Set the proper permissions on the newly extracted folder
```bash
chown www-data: framadate
cd framadate
```

Install dependencies:
```bash
sudo -u www-data composer install
```

# Common part: log file

A log file `admin/stdout.log` must be created and be writable by your web server. Something like that should do it:
```bash
cd framadate
sudo -u www-data touch admin/stdout.log
sudo chmod 600 admin/stdout.log
```

You can now begin to [configure Framadate](Install/Configure).
