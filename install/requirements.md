To install Framadate you'll first need to install :

- Apache 2.4+ (if you use nginx you'll have to use it as a reverse proxy)
- MySQL 5.5.3+
- PHP 5.6+ (PHP 7.0 or more recommended)
- The following PHP extensions: `mbstring`, `intl`, `xml` and `pdo_mysql` or `pdo_pgsql`, depending on what database you've previously chosen.
- If you plan to send emails for notifications, you'll need a configured SMTP server, like Exim4 or Postfix

# Debian

## Stretch (Debian 9)

The commands must be run as root or prefixed with `sudo`. PHP's default version in Stretch is PHP7.0, but you can get newer ones on [Sury's repo](https://deb.sury.org/).

Common packages:
`apt install apache2 php libapache2-mod-php php-intl php-mbstring`

MySQL packages (ignore if you want PostgreSQL):
`apt install mysql-server php-mysql`

PostgreSQL packages (ignore if you want MySQL):
`apt install postgresql php-pgsql`

You can now go to the next step, [configuring your database](Install/Database).
