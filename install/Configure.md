# Configure Framadate

Browse to `https://mywebsite.tld/framadate`. You should arrive on a screen checking the state of the install that will allow you, if every requirement is met, to proceed to the settings install form.

On the next screen, you need to fill the form with the following fields:

- Application name (mandatory): The displayed name of your installation of Framadate
- Administrator mail address (mandatory): The email address of the administrator
- Respond-to mail address (optional): The email specified as recipient when some respond to an email sent by your installation of Framadate
- Default language (mandatory): The language used by default by your installation by Framadate when it can't guess the language of the user
- Clean URL (mandatory): Clean URLs displayed in the browser's address bar. If you enable it, you must setup the htaccess provided (see "Clean URL")
- Connection string (mandatory): The string used to configure the connection to the database.
  - For MySQL, replace `<HOST>` by the host name of your database server (for instance `localhost`). Replace `<SCHEMA>` with the name of the database you previously created (we used `framadate` in a previous part of the documentation). For instance, the result is : "mysql:host=localhost;dbname=framadate;port=3306"
- User (mandatory): The database user used by your installation of Framadate for connecting to the database (we also previously used `framadate`)
- Password (optional): The password of the database user
- Prefix (optional): The prefix of the tables in the database. If you are not sure, don't modify this field.
- Migration table (mandatory): The table used for database migrations. If you are not sure, don't modify this field.

Once the form filled and sent, the file `app/inc/config.php` will be generated on your webserver and you will be redirected to the migration page.

The migration page handles:
- Installing the whole database
- Updating the database when the app has new updates

> Warning: loading the migration page can take a while because it's applying all required database upgrades !


Advanced settings, like the SMTP credentials or the default poll duration, must be manually filled inside the file `app/inc/config.php` on the webserver.

# Server configuration

## Apache

### Clean URL
 To have fancy URLs, activate URL rewriting by activating the provided `.htaccess` file, by renaming it from `htaccess.txt` to `.htaccess`:

```bash
mv htaccess.txt .htaccess
```

For the `.htaccess` to be taken into account by Apache, you must add the following to your VirtualHost file:
```apache
<Directory /var/www/html/framadate>
  AllowOverride All
</Directory>
```
Then reload or restart the Apache server.

You can also put everything that's into the `.htaccess` file directly into your VirtualHost file.

### Secure admin section

The `/admin` folder shouldn't be open to anyone but the admin of the Framadate instance, you should secure it with `.htaccess` and `.htpasswd` files.

Create a `.htaccess` file inside `admin/` and put the following content:

```apache
AuthType Basic
AuthName "Administration"
AuthUserFile "/var/www/html/framadate/admin/.htpasswd"
Require valid-user
```

Then create the `.htpasswd` file with the following command (you may need to install the package `apache2-utils`) where you change the values for user and password to your convenience:
```bash
sudo -u www-data htpasswd -bc /var/www/html/framadate/admin/.htpasswd user password
```

### Secure htaccess and htpassword

In order to disallow read access to the `.htaccess` and `.htpasswd` files, you can add this section to the `.htaccess` file:
```apache
<FilesMatch "^\.ht.*">
deny from all
satisfy all
ErrorDocument 403 "Forbidden"
</FilesMatch>
```

### Secure server
#### HTTPS

It is really advided to use HTTPS and make HTTP redirect to it. Use Let's Encrypt to obtain free certificates.

Make sure the `X-FORWARDED-PROTO` header is passed so that Framadate knows that it needs to generate HTTPS urls.

#### Content Security Policy

If you have access to the configuration of your Framadate web server, it is [a good practice](https://wiki.mozilla.org/Security/Guidelines/Web_Security#Content_Security_Policy) to return a `Content-Security-Policy` header as it prevents different types of XSS attacks.

```apache
<IfModule mod_headers.c>
 Header set Content-Security-Policy "default-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; object-src 'none'; style-src 'self' 'unsafe-inline'; font-src 'self'; img-src 'self'"
</IfModule>
```

#### Referrer-Policy

If someone clicks on an external link inside a survey, the website that link points to will get the survey address in the `Referer` HTTP header. With the `Referrer-Policy`, we can tweak which information is sent.

```apache
<IfModule mod_headers.c>
 Header set Referrer-Policy "strict-origin"
</IfModule>
```
#### Various headers

In the same way that we added the `Content-Security-Policy` and `Referrer-Policy`, we can have :
* `X-Content-Type-Options: nosniff` to avoid XSS attacks through incorrect MIME types
* `X-XSS-Protection: 1; mode=block` to avoid XSS attacks for older browsers

You can also find other security tips here : <https://infosec.mozilla.org/guidelines/web_security>

## Nginx

### Generate authentication file for administration folder

Create the `.htpasswd` file with the following command (you may need to install the package `apache2-utils`) where you change the values for user and password to your convenience:

```bash
sudo -u www-data htpasswd -bc /path/to/framadate/admin/.htpasswd user password
```

Note that the path or name of the .htpasswd does not matter for nginx. put it outside of a web accessible could be a great idea. 

### HTTPS

You can use LetsEncrypt to get certificates.

https://certbot.eff.org/lets-encrypt/debianstretch-nginx

Make sure the `X-FORWARDED-PROTO` header is passed so that Framadate knows that it needs to generate HTTPS urls.

### A working example with HTTPS

```nginx
server {
	listen 80;
	listen [::]:80;
	server_name date.example.org;
	return 301 https://$host$request_uri;
}

server {
        listen 443 ssl http2;
    	 listen [::]:443 ssl http2; # For ipv6 only
        server_name date.example.org;
	
       # This must be adapted if you use cdn or stats scripts.
	add_header Content-Security-Policy "default-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; object-src 'none'; style-src 'self' 'unsafe-inline'; font-src 'self'; img-src 'self'";
       add_header Referrer-Policy "strict-origin";

       # Those ssl certificates is generated by letsencrypt, to create an certificates 
       # through letsencrypt information are available here https://certbot.eff.org/lets-encrypt/debianstretch-nginx
       # For secure ssl configuration, look also at https://mozilla.github.io/server-side-tls/ssl-config-generator/
	include /etc/letsencrypt/options-ssl-nginx.conf;
	ssl_certificate /etc/letsencrypt/live/date.example.org/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/date.example.org/privkey.pem;


        access_log /var/log/nginx/date.example.org.access.log;
        error_log /var/log/nginx/date.example.org.error.log;

        root /path/to/your/framadate;

        index index.php;

       location ~^/(\.git)/{
		deny all;
       }
	location ~ /\. {
		deny all;
	}	

	location ~ ^/composer\.json.*$|^/composer\.lock.*$|^/php\.ini.*$|^/.*\.sh {
		deny all;
	}

        location /admin/ {
		auth_basic "Restricted access";
		auth_basic_user_file /path/to/framadate/admin/.htpasswd;

		location ~ \.php$ {
			fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
			include /etc/nginx/fastcgi_params;
			fastcgi_pass unix:/run/php/php7.0-fpm.sock;
		}
		try_files $uri $uri/ =401; 
	}

        location / {
		rewrite "^/admin$" "/admin/" permanent;	

              # Clean URL
		rewrite "^/([a-zA-Z0-9-]+)$" "/studs.php?poll=$1" last;
		rewrite "^/([a-zA-Z0-9-]+)/action/([a-zA-Z_-]+)/(.+)$" "/studs.php?poll=$1&$2=$3" last;
		rewrite "^/([a-zA-Z0-9-]+)/vote/([a-zA-Z0-9]{16})$" "/studs.php?poll=$1&vote=$2" last;
		rewrite "^/([a-zA-Z0-9]{24})/admin$" "/adminstuds.php?poll=$1" last;
		rewrite "^/([a-zA-Z0-9]{24})/admin/vote/([a-zA-Z0-9]{16})$" "/adminstuds.php?poll=$1&vote=$2" last;
		rewrite "^/([a-zA-Z0-9]{24})/admin/action/([a-zA-Z_-]+)(/([A-Za-z0-9]+))?$" "/adminstuds.php?poll=$1&$2=$4" last;
                try_files $uri /index.php; 
        }

	

        
        location ~ \.php$ {
	    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_index index.php;
            include /etc/nginx/fastcgi_params;
            fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        }
}
```



### Secure server
#### Content Security Policy
```nginx
add_header Content-Security-Policy "default-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; object-src 'none'; style-src 'self' 'unsafe-inline'; font-src 'self'; img-src 'self'";
```

#### Referrer-Policy
```nginx
add_header Referrer-Policy "strict-origin";
```

#### Various headers

In the same way that we added the `Content-Security-Policy` and `Referrer-Policy`, we can have :
* `X-Content-Type-Options: nosniff` to avoid XSS attacks through incorrect MIME types
* `X-XSS-Protection: 1; mode=block` to avoid XSS attacks for older browsers

You can also find other security tips here : <https://infosec.mozilla.org/guidelines/web_security>