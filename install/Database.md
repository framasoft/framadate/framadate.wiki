# Creating database

Framadate's database must be created before it can be populated. We also recommand connecting to your database with a dedicated user to increase security. Therefore you must connect to your MySQL or PostgreSQL server to execute a few commands.

## MySQL

Connect to the database server
```bash
sudo mysql
```

Create the database
```sql
CREATE DATABASE IF NOT EXISTS `framadate` DEFAULT CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;
```

Create a dedicated user (replace `useastrongpassword` with a strong password and don't forget to write it down)
```sql
CREATE USER 'framadate'@'localhost' IDENTIFIED BY 'useastrongpassword';
GRANT ALL PRIVILEGES ON `framadate`.* TO 'framadate'@'localhost';
```
*If the database is on a different server than the application, replace `localhost` with correct value.*

## PostgreSQL

> Framadate **will be** compatible with PostgreSQL only from version 1.2.0, which is yet to be released.

***

Connect to the database server
```bash
su - postgres
psql
```

Create a dedicated user (replace `useastrongpassword` with a strong password and don't forget to write it down)
```sql
CREATE USER 'framadate'@'localhost' WITH PASSWORD 'useastrongpassword';
```

*If the database is on a different server than the application, replace `localhost` with correct value.*

Create the database
```sql
CREATE DATABASE framadate;
```

Grant all rights to the `framadate` user on the `framadate` database.
```sql
GRANT ALL PRIVILEGES ON DATABASE framadate to 'framadate'@'localhost';
```

You can now quit `psql` and the `postgres` user with double `Ctrl` + `D`.

You can begin to [install Framadate](Install/Install).
