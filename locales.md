# Dealing with locales as maintainer

You will need `make`

1. Add your new strings to locale/en.json only. You can add new strings by doing `make add-key-locales "key:string"` (Example: `make add-key-locales "3rd section:This is a new string."`)
1. Push your work
1. When your work is merged, the new strings will automatically be available on <https://weblate.framasoft.org/projects/framadate/>
1. Translate the new strings on Weblate
1. Weblate will open a merge request with the new translations

## How to push translations?

Go to <https://weblate.framasoft.org/projects/framadate/>, go to the right component and language, then `Files` > `Upload translation`
-------------
[Back to homepage](home)